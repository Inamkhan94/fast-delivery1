// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var rms=angular.module('starter', ['ionic', 'starter.controllers','ngMessages','ngCordova','restangular']);

rms.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

rms.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })

     .state('app.welcome', {
        url: '/welcome',
        views: {
          'menuContent': {
            templateUrl: 'templates/welcome.html'
            
          }
        }
      })
      .state('app.signUp', {
        url: '/signUp',
        views: {
          'menuContent': {
            templateUrl: 'templates/signUp.html'

            //controller: 'orderCtrl as vm'
          }
        }
      })
      .state('app.login', {
        url: '/login',
        views: {
          'menuContent': {
            templateUrl: 'templates/login.html'

            //controller: 'orderCtrl as vm'
          }
        }
      })
       .state('app.order', {
        url: '/order',
        views: {
          'menuContent': {
            templateUrl: 'templates/myOrder.html',
            controller: 'orderCtrl as vm'
          }
        }
      })
        .state('app.items', {
        url: '/items',
        views: {
          'menuContent': {
            templateUrl: 'templates/items.html',
            controller: 'itemCtrl as vm'
          }
        }
      })
      .state('app.home', {
        url: '/home',
        views: {
          'menuContent': {
            templateUrl: 'templates/home.html'
           // controller: 'itemCtrl as vm'
          }
        }
      })
      .state('app.goFood', {
        url: '/goFood',
        views: {
          'menuContent': {
            templateUrl: 'templates/goFood.html',
           controller: 'setLocationCtrl as vm'
          }
        }
      })
      .state('app.goMart', {
        url: '/goMart',
        views: {
          'menuContent': {
            templateUrl: 'templates/goMart.html',
           controller: 'martCtrl as vm'
          }
        }
      })
      ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});
