rms.factory('foodList',function(Restangular){
    return {
        getFoodList:getFoodList
    }

    function getFoodList(){
        return Restangular.all('food').getList();
    }
})