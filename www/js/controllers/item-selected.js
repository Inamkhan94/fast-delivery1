/**
 * Created by Inam on 11/22/2016.
 */

rms.controller('itemSelectedCtrl', function () {
  var vm = this;

  vm.quantity = 1;
  vm.itemSelected =
  {
    name: 'Cheese Garlic beard',
    detail: 'Delicious bread baked with cheese',
    price: parseInt('190'),
    currency: 'Rs.'
      }

   vm.total=vm.itemSelected.price;
  vm.increaseQuantity = function () {
    vm.quantity += 1;
    vm.total = vm.itemSelected.price * vm.quantity;

  }
  vm.decreaseQuantity = function () {
    if (vm.quantity <=1)
      return 1;

    vm.quantity -= 1;
    vm.total = vm.total -vm.itemSelected.price;

  }

})
