rms.controller('dailyMenuCtrl',function($scope){
  var vm=this;
   vm.dailyMenu=['Deals','Appetizers','Pizzas','Sandwich','BBG & Grill','Fast Food'];

   $scope.rating={};
   $scope.rating = 3;
  $scope.max_stars = 5;

  $scope.ratingsObject = {
        iconOn: 'ion-ios-star',    //Optional
        iconOff: 'ion-ios-star-outline',   //Optional
        iconOnColor: 'rgb(200, 200, 100)',  //Optional
        iconOffColor:  'rgb(200, 100, 100)',    //Optional
        rating:  2, //Optional
        minRating:1,    //Optional
        readOnly: false, //Optional
        callback: function(rating, index) {    //Mandatory
          $scope.ratingsCallback(rating, index);
        }
      };

      $scope.ratingsCallback = function(rating, index) {
        console.log('Selected rating is : ', rating, ' and the index is : ', index);
      };
    $scope.groups = [];
    for (var i=0; i<1; i++) {
        $scope.groups[i] = {
            name: i,
            items: []
        };
        for (var j=0; j<1; j++) {
            $scope.groups[i].items.push(i + '-' + j);
        }
    }

    /*
     * if given group is the selected group, deselect it
     * else, select the given group
     */
    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = group;
        }
    };
    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };


})
