/**
 * Created by Inam on 11/22/2016.
 */
/**
 * Created by Inam on 11/22/2016.
 */

rms.controller('dealSelectedCtrl', function () {
  var vm = this;

  vm.quantity = 1;
  vm.dealSelected =
  {
    name: 'Deal 1',
    detail: '3 Regular Pizza with Pitcher',
    price: parseInt('1199'),
    currency: 'Rs.'
  }

  vm.total=vm.dealSelected.price;
  vm.increaseQuantity = function () {
    vm.quantity += 1;
    vm.total = vm.dealSelected.price * vm.quantity;

  }
  vm.decreaseQuantity = function () {
    if (vm.quantity <=1)
      return 1;

    vm.quantity -= 1;
    vm.total = vm.total -vm.dealSelected.price;

  }

})
