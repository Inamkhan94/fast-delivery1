/**
 * Created by Inam on 11/21/2016.
 */

rms.controller('setLocationCtrl',function($cordovaGeolocation){
   var vm=this;
     vm.location={};
     vm.setLocation=setLocation;





  var posOptions = {timeout: 1000000000, enableHighAccuracy: false};
  $cordovaGeolocation
    .getCurrentPosition(posOptions)

    .then(function (position) {
      vm.lat  = position.coords.latitude
      vm.long = position.coords.longitude
      vm.setLocation();

    }, function(err) {
      console.log(err)
    });

  vm.watchOptions = {timeout : 3000, enableHighAccuracy: false};
  vm.watch = $cordovaGeolocation.watchPosition(vm.watchOptions);

  vm.city,vm.area;

  function setLocation() {
    vm.map = new google.maps.Map(document.getElementById('abc'), {
      zoom: 8,
      center: {lat: vm.lat, lng: vm.long}
    });
    vm.geocoder = new google.maps.Geocoder;
    vm.infowindow = new google.maps.InfoWindow;

    document.getElementById('submit').addEventListener('click', function() {
      geocodeLatLng(vm.geocoder,vm.map,vm.infowindow);
    });
  }

  function geocodeLatLng(geocoder, map, infowindow) {
    vm.input = vm.lat + ',' + vm.long;
    vm.latlngStr = vm.input.split(',', 2);
    vm.latlng = {lat: parseFloat(vm.latlngStr[0]), lng: parseFloat(vm.latlngStr[1])};
    vm.geocoder.geocode({'location': vm.latlng}, function(results, status) {
      if (status === 'OK') {
        if (results[1]) {
          vm.map.setZoom(11);
          vm.marker = new google.maps.Marker({
            position: vm.latlng,
            map: map
          });

          vm.info=results[1].formatted_address.split(',');
          vm.city=vm.info[1];
          vm.area=vm.info[0];
          vm.infowindow.setContent(results[1].formatted_address);
          vm.infowindow.open(vm.map, vm.marker);
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }



 });
