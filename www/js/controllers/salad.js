/**
 * Created by Inam on 11/22/2016.
 */

rms.controller('saladCtrl', function () {
  var vm = this;

  vm.quantity = 1;
  vm.salad=
  {
    name: 'Salad',
    detail: 'Combination of Macronis mixed with Cheddar Cheese,Spagetti,Coleslow Salad,Potato Salad,Russian Salad',
    regularPrice: parseInt('199'),
    largePrice: parseInt('299'),
    currency: 'Rs.'
  }


   vm.increaseQuantity = function () {
    vm.quantity += 1;
    vm.total = vm.newVal * vm.quantity;

  }

  vm.changeMyOpt = function (val) {
    if(val == 0){
      vm.newVal = vm.salad.regularPrice ;
      vm.total = vm.newVal * vm.quantity;
    }
    else {
      vm.newVal = vm.salad.largePrice;
        vm.total = vm.newVal * vm.quantity;
    }

  };


  vm.decreaseQuantity = function () {
    if (vm.quantity <=1)
      return 1;
    vm.quantity -= 1;
    vm.total = vm.total -vm.newVal;

  }


})
